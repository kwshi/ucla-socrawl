import sqlite3
import csv
import os


out_path = "csv"


def export_table(conn: sqlite3.Connection, table: str):
    with open(os.path.join(out_path, f"{table}.csv"), "w") as f:
        writer = csv.writer(f)

        cursor = conn.execute(f"pragma table_info({table});")
        columns = [name for _, name, _, _, _, _ in cursor]
        writer.writerow(columns)

        cursor = conn.execute(f"select * from {table};")
        for row in cursor:
            writer.writerow(row)


def main():
    os.makedirs(out_path, exist_ok=True)
    with sqlite3.connect("./scrape/data.db") as conn:
        export_table(conn, "subject")
        export_table(conn, "course")
        export_table(conn, "section")
        export_table(conn, "schedule")


if __name__ == "__main__":
    main()
