import sqlite3
import csv
import os

query = """
SELECT
    schedule.subject,
    course.name,
    schedule.section,
    schedule.instructor,
    schedule.location,
    (
        CASE WHEN schedule.days >> 0 & 1 THEN 'U' ELSE '' END ||
        CASE WHEN schedule.days >> 1 & 1 THEN 'M' ELSE '' END ||
        CASE WHEN schedule.days >> 2 & 1 THEN 'T' ELSE '' END ||
        CASE WHEN schedule.days >> 3 & 1 THEN 'W' ELSE '' END ||
        CASE WHEN schedule.days >> 4 & 1 THEN 'R' ELSE '' END ||
        CASE WHEN schedule.days >> 5 & 1 THEN 'F' ELSE '' END ||
        CASE WHEN schedule.days >> 6 & 1 THEN 'S' ELSE '' END
    ) AS days,
    schedule.time_start / 60 AS start_hour,
    schedule.time_start % 60 AS start_minute,
    section.status_enrolled,
    section.status_capacity
FROM schedule
INNER JOIN section ON
    schedule.term = section.term AND
    schedule.subject = section.subject AND
    schedule.catalog_number = section.catalog_number AND
    schedule.class_number = section.class_number AND
    schedule.section = section.section
INNER JOIN course ON
    schedule.term = course.term AND
    schedule.subject = course.subject AND
    schedule.catalog_number = course.catalog_number AND
    schedule.class_number = course.class_number
WHERE
    schedule.days >> 4 & 1 AND
    status_enrolled > 0
ORDER BY status_enrolled DESC
"""


def main():
    with sqlite3.connect("./scrape/data.db") as conn, open(
        os.path.join("csv", "thursday.csv"), "w"
    ) as out:
        cursor = conn.execute(query)
        writer = csv.writer(out)

        fields = [column for column, *_ in cursor.description]
        writer.writerow(fields)

        for row in cursor:
            writer.writerow(row)


if __name__ == "__main__":
    main()
