import Sqlite3 from "better-sqlite3";
import * as Drizzle from "drizzle-orm";
import * as DrizzleSqlite3 from "drizzle-orm/better-sqlite3";
import pino from "pino";

import * as Api from "./api";
import * as Schema from "./schema";

const term = "24f";
const dbPath = "data.db";

const logger = pino();

const sqlite = new Sqlite3(dbPath);
const db = DrizzleSqlite3.drizzle(sqlite, { schema: Schema });

const refreshSubjectsList = async () => {
	const subjects = await Api.Subject.list(term);
	await db
		.insert(Schema.subject)
		.values(subjects.map((subject) => ({ term, ...subject })))
		.onConflictDoNothing();
};

const getStaleSubjects = () =>
	db.query.subject.findMany({
		where: (subject) => Drizzle.isNull(subject.done),
	});

const getStaleCourses = (subject: typeof Schema.subject.$inferSelect) =>
	db
		.select()
		.from(Schema.course)
		.leftJoin(
			Schema.section,
			Drizzle.and(
				Drizzle.eq(Schema.course.term, Schema.section.term),
				Drizzle.eq(Schema.course.subject, Schema.section.subject),
				Drizzle.eq(Schema.course.catalogNumber, Schema.section.catalogNumber),
				Drizzle.eq(Schema.course.classNumber, Schema.section.classNumber),
			),
		)
		.where(
			Drizzle.and(
				Drizzle.eq(Schema.course.term, subject.term),
				Drizzle.eq(Schema.course.subject, subject.code),
				Drizzle.isNull(Schema.section.term),
			),
		);

const markSubjectDone = async (subject: typeof Schema.subject.$inferSelect) =>
	await db
		.update(Schema.subject)
		.set({ done: Drizzle.sql`(unixepoch())` })
		.where(
			Drizzle.and(
				Drizzle.eq(Schema.subject.term, term),
				Drizzle.eq(Schema.subject.code, subject.code),
			),
		);

const refreshSubject = async (subject: typeof Schema.subject.$inferSelect) => {
	logger.info(
		"listing courses %s/%s [%s]",
		subject.term,
		subject.code,
		subject.name,
	);
	const courses = await Api.Course.list(term, subject.code);
	if (!courses.length) {
		await markSubjectDone(subject);
		return;
	}

	await db
		.insert(Schema.course)
		.values(
			courses.map((course) => ({
				term: subject.term,
				subject: subject.code,
				...course,
			})),
		)
		.onConflictDoNothing();

	const stale = await getStaleCourses(subject);
	for (const entry of stale) await refreshCourse(entry.course);
	await markSubjectDone(subject);
};

const statusToDb = (status: Api.Summary.Summary["status"]) => {
	switch (status.tag) {
		case "open":
			return {
				status: "open",
				statusEnrolled: status.enrolled,
				statusCapacity: status.capacity,
			} as const;
		case "full":
			return {
				status: "full",
				statusEnrolled: status.capacity,
				statusCapacity: status.capacity,
			} as const;
		case "cancelled":
			return { status: "cancelled" } as const;
	}
};

const saveSummary = async (
	course: typeof Schema.course.$inferSelect,
	entry: Api.Summary.Summary,
) => {
	const key = {
		term: course.term,
		subject: course.subject,
		catalogNumber: course.catalogNumber,
		classNumber: course.classNumber,
		section: entry.section,
	};
	const dbEntry = {
		...statusToDb(entry.status),
		fresh: Drizzle.sql`(unixepoch())`,
	};
	await db
		.insert(Schema.section)
		.values({ ...key, ...dbEntry })
		.onConflictDoUpdate({
			target: [
				Schema.section.term,
				Schema.section.subject,
				Schema.section.catalogNumber,
				Schema.section.classNumber,
				Schema.section.section,
			],
			set: dbEntry,
		});

	await db.insert(Schema.schedule).values(
		entry.schedule.map((s) => ({
			...key,
			instructor: s.instructor,
			location: s.location,
			days: s.days,
			timeStart: s.time?.start,
			timeEnd: s.time?.end,
		})),
	);
};

const refreshCourse = async (course: typeof Schema.course.$inferSelect) => {
	logger.info(
		"fetching details %s/%s/%s/%s",
		course.term,
		course.subject,
		course.catalogNumber,
		course.classNumber,
	);

	const details = await Api.Summary.list(
		course.term,
		course.subject,
		course.catalogNumber,
		course.classNumber,
		"lecture",
	);
	for (const entry of details) {
		await saveSummary(course, entry);

		logger.info(
			"fetching sub-details %s/%s/%s/%s",
			course.term,
			course.subject,
			course.catalogNumber,
			entry.uclaNumber,
		);
		for (const sub of await Api.Summary.list(
			course.term,
			course.subject,
			course.catalogNumber,
			entry.uclaNumber,
			"lecture",
		)) {
			await saveSummary(course, sub);
		}
	}
};

const main = async () => {
	logger.info("refreshing subjects");
	await refreshSubjectsList();

	//const subjects = await getStaleSubjects();
	//for (const subject of subjects) {
	//	await refreshSubject(subject);
	//}

	await refreshSubject({ term: "24f", code: "MATH", name: "math", done: null });
	await refreshSubject({
		term: "24f",
		code: "COMPTNG",
		name: "math",
		done: null,
	});

	//console.log(await apiListCourses("24s", "AF AMER"));
	//console.log(await apiShowDetails("24s", "MATH", "0031B", "lecture"));
};

await main();
