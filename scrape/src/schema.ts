import * as Ds from "drizzle-orm/sqlite-core";

const subjectKeySpec = {
	term: Ds.text("term").notNull(),
	code: Ds.text("code").notNull(),
};

const courseKeySpec = {
	term: Ds.text("term").notNull(),
	subject: Ds.text("subject").notNull(),
	catalogNumber: Ds.text("catalog_number").notNull(),
	classNumber: Ds.text("class_number").notNull(),
};

const sectionKeySpec = { ...courseKeySpec, section: Ds.text("section") };

export const subject = Ds.sqliteTable(
	"subject",
	{
		...subjectKeySpec,
		name: Ds.text("name").notNull(),
		done: Ds.integer("done", { mode: "timestamp" }),
	},
	(subject) => ({
		pk: Ds.primaryKey({ columns: [subject.term, subject.code] }),
	}),
);

export const course = Ds.sqliteTable(
	"course",
	{
		...courseKeySpec,
		name: Ds.text("name").notNull(),
	},
	(course) => ({
		subjectReference: Ds.foreignKey({
			columns: [course.term, course.subject],
			foreignColumns: [subject.term, subject.code],
		}),
		pk: Ds.primaryKey({
			columns: [
				course.term,
				course.subject,
				course.catalogNumber,
				course.classNumber,
			],
		}),
	}),
);

export const section = Ds.sqliteTable(
	"section",
	{
		...sectionKeySpec,
		status: Ds.text("status", {
			enum: ["open", "full", "cancelled"],
		}).notNull(),
		statusEnrolled: Ds.integer("status_enrolled"),
		statusCapacity: Ds.integer("status_capacity"),
		fresh: Ds.integer("fresh", { mode: "timestamp" }),
	},
	(section) => ({
		pk: Ds.primaryKey({
			columns: [
				section.term,
				section.subject,
				section.catalogNumber,
				section.classNumber,
				section.section,
			],
		}),
		courseReference: Ds.foreignKey({
			name: "section_course",
			columns: [
				section.term,
				section.subject,
				section.catalogNumber,
				section.classNumber,
			],
			foreignColumns: [
				course.term,
				course.subject,
				course.catalogNumber,
				course.classNumber,
			],
		}),
	}),
);

export const schedule = Ds.sqliteTable(
	"schedule",
	{
		...sectionKeySpec,
		instructor: Ds.text("instructor"),
		location: Ds.text("location"),
		days: Ds.integer("days"),
		timeStart: Ds.integer("time_start"),
		timeEnd: Ds.integer("time_end"),
	},
	(schedule) => ({
		sectionReference: Ds.foreignKey({
			name: "schedule_section",
			columns: [
				schedule.term,
				schedule.subject,
				schedule.catalogNumber,
				schedule.classNumber,
				schedule.section,
			],
			foreignColumns: [
				section.term,
				section.subject,
				section.catalogNumber,
				section.classNumber,
				section.section,
			],
		}),
	}),
);
