import * as Vt from "vitest";

import { list } from "./summary";

Vt.test("list 24f-math-31a discussions", async () => {
	await list("24f", "MATH", "0031A", "%", "lecture");
	await list("24f", "MATH", "0031A", " 001", "discussion");
});
