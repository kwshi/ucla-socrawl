import * as Z from "zod";

export const List = Z.array(
	Z.object({ label: Z.string(), value: Z.string().length(7) }).transform((entry, ctx) => {
    const code = entry.value.trimEnd()

    const match = entry.label.match(/^(.*) \(([A-Z& \-]+)\)$/)
    if (!match) {

    if (entry.label.toUpperCase() !== code)  {    ctx.addIssue({code: Z.ZodIssueCode.custom, message: 'malformed label', params: {entry}})
      return Z.NEVER
    }
    return {name: entry.label, code}
    }

    const [,name,labelCode] = match as [string, string, string]
    if (labelCode !== code) {
      ctx.addIssue({code: Z.ZodIssueCode.custom, message: 'code in label does not match value'})
      return Z.NEVER
    }

    return {name, code}
  }),
);
export type List = Z.TypeOf<typeof List>;

export const list = async (term: string) => {
	const url = new URL(
		"https://sa.ucla.edu/ro/ClassSearch/Public/Search/GetLevelSeparatedSearchData",
	);
	url.searchParams.set("input", JSON.stringify({ term_cd: term }));
	url.searchParams.set("level", "1");
	const response = await fetch(url);
	return List.parse(await response.json());
};
