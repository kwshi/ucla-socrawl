import * as Z from 'zod'
import * as P5 from 'parse5'
import * as P5Tools from '@parse5/tools'

import * as Html from'./lib/html'

export const Schedule = Z.object({
	instructor: Z.string(),
	location: Z.string(),
	days: Z.number().int(),
	time: Z.nullable(
		Z.object({ start: Z.number().int(), end: Z.number().int() }),
	),
});
export type Schedule = Z.TypeOf<typeof Schedule>;

export const Detail = Z.object({
  status: Z.discriminatedUnion("tag", [
		Z.object({
			tag: Z.literal("open"),
			enrolled: Z.number(),
			capacity: Z.number(),
		}),
		Z.object({
			tag: Z.literal("full"),
			enrolled: Z.number(),
			capacity: Z.number(),
		}),
		Z.object({ tag: Z.literal("cancelled") }),
	]),
	schedule: Z.array(Schedule),
})


export const get = async (
  term: string,
classId: string, 
  subject: string, catalogNumber: string, classNumber: string
                         ) => {
  // https://sa.ucla.edu/ro/Public/SOC/Results/ClassDetail?term_cd=24F&subj_area_cd=MATH%20%20%20&crs_catlg_no=0031A%20%20%20&class_id=262181201&class_no=%20001%20%20
  // https://sa.ucla.edu/ro/Public/SOC/Results/ClassDetail?term_cd=24f&subj_area_cd=math&crs_catlg_no=0031a&class_id=262181201&class_no=%20001
  const url = new URL('https://sa.ucla.edu/ro/Public/SOC/Results/ClassDetail')
    url.searchParams.set('term_cd', term)
  url.searchParams.set('subj_area_cd', subject)
  url.searchParams.set('crs_catlg_no', catalogNumber)
  url.searchParams.set('class_id', classId)
  url.searchParams.set('class_no', classNumber)

  const response = await fetch(url)
  const html = await response.text()
  const document = P5.parseFragment(html)
  const rootTemplate = Html.getElementById(document, 'ucla-sa-soc-app')

  if (!rootTemplate ||!Html.isTemplateElement(rootTemplate))
    throw new Error('missing ucla-sa-soc-app template')

  const rootFragment = rootTemplate.content

  const summaryInfo = Html.getElementById(rootFragment, 'enrl_mtng_info')
  if (!summaryInfo) throw new Error('missing summary info section')

  const headRow = Html.findElementByTagPath(summaryInfo,['thead', 'tr'])
  const bodyRow = Html.findElementByTagPath(summaryInfo,['tbody', 'tr'])
  if (!headRow) throw new Error('missing table header')
  if (!bodyRow) throw new Error('missing table body')

  const x = Html.getChildrenTextJoin(headRow)
  const y = Html.getChildrenTextJoin(bodyRow)
  console.log(y)


}
