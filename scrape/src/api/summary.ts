import * as Z from "zod";
import * as P5 from "parse5";

import * as Html from "./lib/html";
import { parseDays } from "./lib/days";
import { parseTimeRange } from "./lib/time";

export const Schedule = Z.object({
	instructor: Z.string(),
	location: Z.string(),
	days: Z.number().int(),
	time: Z.nullable(
		Z.object({ start: Z.number().int(), end: Z.number().int() }),
	),
});
export type Schedule = Z.TypeOf<typeof Schedule>;

//{
//  Term: '24F',
//  SubjectAreaCode: 'MATH   ',
//  CatalogNumber: '0031A   ',
//  IsRoot: false,
//  SessionGroup: null,
//  ClassNumber: ' 004  ',
//  SequenceNumber: '1',
//  Path: '262181230_F_BLOCK',
//  MultiListedClassFlag: 'n',
//  Token: 'MDAzMUEgICAyNjIxODEyMzBfRl9CTE9DSw=='
//}

const Metadata = Z.object({
	Term: Z.string(),
	SubjectAreaCode: Z.string(),
	CatalogNumber: Z.string(),
	ClassNumber: Z.string(),
	Path: Z.string(),
	MultiListedClassFlag: Z.enum(["n"]),
	Token: Z.string(),
	SequenceNumber: Z.string(),
	// on some sample data, this is just `btoa('0031A   262181206_F_BLOCK')`
});

export const Summary = Z.object({
	section: Z.string(),
	uclaId: Z.number().int(),
	uclaNumber: Z.string(),
	status: Z.discriminatedUnion("tag", [
		Z.object({
			tag: Z.literal("open"),
			enrolled: Z.number(),
			capacity: Z.number(),
		}),
		Z.object({
			tag: Z.literal("full"),
			enrolled: Z.number(),
			capacity: Z.number(),
		}),
		Z.object({ tag: Z.literal("cancelled") }),
	]),
	schedule: Z.array(Schedule),
});
export type Summary = Z.TypeOf<typeof Summary>;

const parseRowElement = (row: P5.DefaultTreeAdapterMap["element"]) => {
	const schedule: Partial<{
		days: string[];
		time: string[][];
		location: string[];
		instructor: string[];
	}> = {};

	const script = Html.findElementByTag(row, "script");
	if (!script) throw Error("missing script element");
	const scriptText = Html.getText(script).join("");

	const match = scriptText.match(
		/^var addCourse(\d{9})_\w+\s*=\s*function\s*\(\)\s*\{\s*Iwe_ClassSearch_SearchResults\.AddToCourseData\("(\d{9})_\w+",(\{[^{}]*\})\);\s*\};/,
	);
	if (!match) throw Error("malformed script element");

	const uclaId = Number.parseInt(match[1]!, 10);
	const data = Metadata.parse(JSON.parse(match[3]!));

	const partial: Partial<Summary> = {
		uclaId,
		uclaNumber: data.ClassNumber,
	};

	for (const column of row.childNodes) {
		if (!P5.defaultTreeAdapter.isElementNode(column)) continue;
		const classes = Html.getClassNames(column);
		const text = Html.getText(column);
		switch (true) {
			case classes.has("sectionColumn"): {
				// the HTML for this column contains two duplicate elements,
				// one with class `hide-small` and the other `hide-small-above`.
				// they have the same content. if this assertion doesn’t hold
				// then the HTML format has changed, and we need to update this
				// parsing logic to match it.
				if (text.length !== 2 || text[0] !== text[1])
					throw Error("invalid sectionColumn format");
				partial.section = (text as [string, string])[0];
				continue;
			}

			case classes.has("statusColumn"):
				switch (text[0]) {
					case "Cancelled":
						partial.status = { tag: "cancelled" };
						continue;
					case "Waitlist":
					case "Closed": {
						if (text.length === 1) {
							partial.status = { tag: "full", capacity: 0, enrolled: 0 };
							continue;
						}
						if (text.length !== 2)
							throw new Error(`invalid statusColumn ${JSON.stringify(text)}`);
						const match = text[1]?.match(
							/^Class Full \((\d+)\)(?:, Over Enrolled By (\d+))?$/,
						);
						if (!match) throw new Error("invalid statusColumn pattern");
						const [, capacityString, overEnrolled] = match as [
							string,
							string,
							string | null,
						];
						const capacity = Number.parseInt(capacityString, 10);
						const enrolled =
							capacity + (overEnrolled ? Number.parseInt(overEnrolled, 10) : 0);
						partial.status = { tag: "full", capacity, enrolled };
						continue;
					}
					case "Open": {
						if (text.length !== 3) throw new Error("invalid statusColumn");
						const match = text[1]?.match(/^(\d+) of (\d+) Enrolled$/);
						if (!match) throw new Error("invalid statusColumn");
						const [, enrolled, capacity] = match as [string, string, string];
						partial.status = {
							tag: "open",
							enrolled: Number.parseInt(enrolled, 10),
							capacity: Number.parseInt(capacity, 10),
						};
						continue;
					}
					case "Closed by Dept": {
						if (text.length !== 3 && text.length !== 2)
							throw new Error("invalid statusColumn closedByDept");
						const match = text[text.length - 1]?.match(
							/^\((\d+) capacity, (\d+) enrolled, (\d+) waitlisted\)$/,
						);
						if (!match) throw new Error("invalid statusColumn");
						const open = Number.parseInt(match[1]!, 10);
						const enrolled = Number.parseInt(match[2]!, 10);
						partial.status = {
							tag: "full",
							enrolled,
							capacity: open + enrolled,
						};
						continue;
					}
					default:
						throw Error(
							`unrecognized statusColumn type ${JSON.stringify(text)}`,
						);
				}

			case classes.has("instructorColumn"):
				schedule.instructor = text;
				continue;
			case classes.has("dayColumn"):
				schedule.days = text;
				continue;
			case classes.has("timeColumn"): {
				const lines = Html.getTextSplitBr(column);
				schedule.time = lines;
				continue;
			}
			case classes.has("locationColumn"):
				schedule.location = text;
				continue;

			// these columns contain stuff that may be useful someday…
			case classes.has("unitsColumn"):
				continue;
			// these columns exist but don’t really contain useful stuff
			case classes.has("waitlistColumn"):
			case classes.has("infoColumn"):
				continue;
		}
	}

	if (
		!schedule.days ||
		!schedule.time ||
		!schedule.location ||
		!schedule.instructor
	)
		throw new Error("missing schedule");
	if (
		schedule.days.length < schedule.time.length ||
		schedule.time.length > schedule.location.length
	)
		throw new Error("schedule count mismatch");

	partial.schedule = [];
	for (const instructor of schedule.instructor)
		for (let i = 0; i < schedule.time.length; ++i) {
			partial.schedule.push({
				days: parseDays(schedule.days[i]!),
				time: parseTimeRange(schedule.time[i]!.join("")),
				location: schedule.location[i]!,
				instructor,
			});
		}

	return Summary.parse(partial);
};

export const list = async (
	term: string,
	subject: string,
	catalogNumber: string,
	classNumber: string,
	_show: "lecture" | "discussion",
) => {
	const url = new URL(
		"https://sa.ucla.edu/ro/public/soc/Results/GetCourseSummary",
	);
	const idPrefix = "F_BLOCK";
	url.searchParams.set(
		"model",
		JSON.stringify({
			Term: term,
			SubjectAreaCode: subject,
			CatalogNumber: catalogNumber,
			ClassNumber: classNumber,
			// i don't know if this is still useful at all
			//IsRoot: false,
			Path: idPrefix,
		}),
	);
	url.searchParams.set("FilterFlags", "{}");
	const response = await fetch(url);
	const html = await response.text();
	const rootFragment = P5.parseFragment(html);
	const entries = Html.getElementById(rootFragment, `${idPrefix}-children`);
	// no results, e.g. no discussion sections
	if (!entries) return [];

	const rows: Summary[] = [];
	for (const entry of entries.childNodes) {
		if (Html.isBlankNode(entry)) continue;
		if (!P5.defaultTreeAdapter.isElementNode(entry))
			throw new Error("unexpected non-element");
		rows.push(parseRowElement(entry));
	}
	return rows;
};
