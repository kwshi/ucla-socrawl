import * as Z from "zod";

export const List = Z.array(
	Z.object({
		label: Z.string(),
		value: Z.object({ crs_catlg_no: Z.string(), class_no: Z.string() }),
	}).transform((entry, ctx) => {
    const match = entry.label.match(/^([0-9A-Z]+) - (.*)$/)
    if (!match) {
      ctx.addIssue({
        code: Z.ZodIssueCode.custom,
        message: 'malformed label'
      })
      return Z.NEVER
    }

    const [, code, name] = match as [string, string, string]

    return {
		name,
    code,
		catalogNumber: entry.value.crs_catlg_no,
		classNumber: entry.value.class_no,
    }
	}),
);
export type List = Z.TypeOf<typeof List>;

export const list = async (term: string, subject: string) => {
	const url = new URL(
		"https://sa.ucla.edu/ro/ClassSearch/Public/Search/GetLevelSeparatedSearchData",
	);
	url.searchParams.set(
		"input",
		JSON.stringify({
			search_by: "subject",
			term_cd: term,
			subj_area_cd: subject,
			ses_grp_cd: "%",
		}),
	);
	url.searchParams.set("level", "2");
	const response = await fetch(url);
	const text = await response.text();
	if (!text) return [];
	return List.parse(JSON.parse(text));
};


