import * as Vt from "vitest";
import { list } from "./course";

Vt.test("list courses 24s-math", async () => {
	const courses = await list("24s", "MATH");

	for (const course of courses) {
		const match = course.code.match(/^([A-Z]*)(\d+)([A-Z]{0,2})/);
		Vt.expect(match, course.code).toBeTruthy();
		if (!match) continue;

		const [, prefix, number, suffix] = match as [
			string,
			string,
			string,
			string,
		];

		const catalogInitial = `${number.padStart(4, "0")}${suffix}`;
		const catalog = prefix
			? `${catalogInitial.padEnd(6)}${prefix}`
			: catalogInitial;

		Vt.expect(course.catalogNumber).toBe(catalog);

		Vt.expect(course.classNumber).toMatch(/^%| \d{3} {2}$/);
	}
});
