import * as Vt from 'vitest'

import {list} from './subject'

const examples = [{code: 'MATH', name: 'Mathematics'},
  {code: 'COM SCI', name: 'Computer Science'},
{code: 'DANCE', name:'Dance'},
]

Vt.test('list subjects 24s', async () => {
  const subjects = await list('24s');

  const countCode= new Map<string, number>()
  const countName= new Map<string, number>()
  for (const subject of subjects) {
    countCode.set(subject.code, (countCode.get(subject.code) ?? 0) + 1)
    countName.set(subject.name, (countCode.get(subject.name) ?? 0) + 1)
  }

  for (const [code, count] of countCode.entries()) Vt.expect(count, code).toBe(1)
  for (const [name, count] of countName.entries()) Vt.expect(count, name).toBe(1)

  for (const example of examples) Vt.expect(subjects).toContainEqual(example)
})
