import * as P5 from "parse5";
import * as P5Tools from "@parse5/tools";

type TupleNonempty<T> = [T, ...T[]];
type Tuple<T> = TupleNonempty<T> | [];

interface Pattern<T> {
	parse: (node: P5Tools.Node) => T;
}

const enum ItemType {
	node,
	text,
}

type ItemSpecNode<T> = { type: ItemType.node; pattern: Pattern<T> };
type ItemSpecText = { type: ItemType.text };

type ItemSpec = ItemSpecNode<unknown> | ItemSpecText;

type ItemOutput<Spec extends ItemSpec> = Spec extends ItemSpecNode<infer T>
	? T
	: Spec extends ItemSpecText
		? string
		: never;

type ElementObjectSpecCons<Key extends string, Value extends ItemSpec, Prev> = {
	key: Key;
	value: Value;
	prev: Prev;
};

type ElementObjectSpec = {
	key: string;
	value: ItemSpec;
	prev: ElementObjectSpec;
} | null;

type ElementObjectOutput<Spec> = Spec extends ElementObjectSpecCons<
	infer Key,
	infer Value,
	infer Prev
>
	? { [_ in Key]: ItemOutput<Value> } & ElementObjectOutput<Prev>
	: {};

class ElementObjectPattern<const Spec extends ElementObjectSpec>
	implements Pattern<ElementObjectOutput<Spec>>
{
	readonly tag: string;
	readonly spec: Spec;

	constructor(tag: string, spec: Spec) {
		this.tag = tag;
		this.spec = spec;
	}

	#push<const K extends string, Item extends ItemSpec>(
		key: K,
		value: Item,
	): ElementObjectPattern<{ key: K; value: Item; prev: Spec }> {
		return new ElementObjectPattern(this.tag, { key, value, prev: this.spec });
	}

	childNode<const K extends string, V>(
		key: K,
		value: Pattern<V>,
	): ElementObjectPattern<{ key: K; value: ItemSpecNode<V>; prev: Spec }> {
		return this.#push(key, { type: ItemType.node, pattern: value });
	}

	textContent<const K extends string>(
		key: K,
	): ElementObjectPattern<{ key: K; value: ItemSpecText; prev: Spec }> {
		return this.#push(key, { type: ItemType.text });
	}

	#specArray() {
		const items: { key: string; value: ItemSpec }[] = [];
		let current: ElementObjectSpec = this.spec;
		while (current) {
			items.push({ key: current.key, value: current.value });
			current = current.prev;
		}
		return items.reverse();
	}

	parse(node: P5Tools.Node) {
		if (!P5Tools.isElementNode(node))
			throw new Error(`not element ${this.tag} ${node.nodeName}`);
		if (node.tagName !== this.tag) throw new Error("tag mismatch");

		const output: Record<string, unknown> = {};
		const children = node.childNodes[Symbol.iterator]();

		// TODO detect and error on extra trailing child nodes

		for (const entry of this.#specArray()) {
			for (;;) {
				const next = children.next();
				if (next.done) throw new Error("premature end");

				const child = next.value;
				if (P5Tools.isTextNode(child) && !child.value.trim()) continue;

				switch (entry.value.type) {
					case ItemType.node: {
						const result = entry.value.pattern.parse(child);
						output[entry.key] = result;
						break;
					}
					case ItemType.text: {
						output[entry.key] = P5Tools.getTextContent(child);
						break;
					}
				}
				break;
			}
		}

		return output as ElementObjectOutput<Spec>;
	}
}

interface ElementArrayPattern<T extends Tuple<unknown>> extends Pattern<T> {
	childNode: <V>(item: Pattern<V>) => ElementArrayPattern<[...T, V]>;
	textContent: () => ElementArrayPattern<[...T, string]>;
}

class ElementSinglePattern<T> implements Pattern<T> {
	readonly tag: string;
	readonly child: Pattern<T>;

	constructor(tag: string, child: Pattern<T>) {
		this.tag = tag;
		this.child = child;
	}

	parse(node: P5Tools.Node): T {
		if (!P5Tools.isElementNode(node))
			throw new Error(`not element ${this.tag} ${node.nodeName}`);
		if (node.tagName !== this.tag) throw new Error("tag mismatch");

		// todo handle having too many children
		for (const child of node.childNodes) {
			if (P5Tools.isTextNode(child) && !child.value.trim()) continue;

			return this.child.parse(child);
		}

		// none found
		throw new Error("missing child");
	}
}

class ElementManyPattern<T> implements Pattern<T[]> {
	readonly tag: string;
	readonly item: Pattern<T>;
	constructor(tag: string, item: Pattern<T>) {
		this.tag = tag;
		this.item = item;
	}

	parse(node: P5Tools.Node): T[] {
		if (!P5Tools.isElementNode(node))
			throw new Error(`not element ${this.tag} ${node.nodeName}`);
		if (node.tagName !== this.tag) throw new Error("tag mismatch");

		const results = [];
		for (const child of node.childNodes) {
			if (P5Tools.isTextNode(child) && !child.value.trim()) continue;

			const result = this.item.parse(child);
			results.push(result);
		}
		return results;
	}
}

class ElementTextPattern<T> implements Pattern<string> {
	readonly tag: string;
	constructor(tag: string) {
		this.tag = tag;
	}

	parse(node: P5Tools.Node): string {
		if (!P5Tools.isElementNode(node))
			throw new Error(`not element ${this.tag} ${node.nodeName}`);
		if (node.tagName !== this.tag) throw new Error("tag mismatch");

		return P5Tools.getTextContent(node);
	}
}

class ElementBuilder implements ElementBuilder {
	readonly tag: string;

	constructor(tag: string) {
		this.tag = tag;
	}

	object(): ElementObjectPattern<null> {
		return new ElementObjectPattern(this.tag, null);
	}
	array(): ElementArrayPattern<[]> {}

	childNode<T>(child: Pattern<T>) {
		return new ElementSinglePattern(this.tag, child);
	}

	childNodes<T>(item: Pattern<T>) {
		return new ElementManyPattern(this.tag, item);
	}

	textContent() {
		return new ElementTextPattern(this.tag);
	}
}

export const element = (tag: string) => new ElementBuilder(tag);
