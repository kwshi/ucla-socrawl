const dayIndex = { U: 0, M: 1, T: 2, W: 3, R: 4, F: 5, S: 6 } as const;
export const parseDays = (daysString: string) => {
	if (daysString === "Not scheduled" || daysString === "Varies") return 0;
	let n = 0;
	for (const c of daysString) {
		if (!(c in dayIndex)) throw Error(`invalid day char ${c}`);
		n |= 1 << dayIndex[c as keyof typeof dayIndex];
	}
	return n;
};


