export const parseTime = (timeString: string) => {
	const match = timeString.match(/^-?(\d+)(?::(\d+))?([ap])m$/);
	if (!match) throw new Error(`bad time string ${timeString}`);
	const [, hour, minute, ap] = match as [
		string,
		string,
		string | null,
		"a" | "p",
	];
	return (
		(Number.parseInt(hour, 10) + 12 * +(ap === "p")) * 60 +
		Number.parseInt(minute ?? "0", 10)
	);
};

export const parseTimeRange = (s: string) => {
	if (!s || s === "To be arranged" || s === "---") return null;

	const match = s.match(/^(\d+)(?::(\d+))?([ap])m-(\d+)(?::(\d+))?([ap])m$/);
	if (!match) throw new Error(`bad time range string ${s}`);
	const [, startH, startM, startAp, endH, endM, endAp] = match as [
		string,
		string,
		string | null,
		"a" | "p",
		string,
		string | null,
		"a" | "p",
	];
	return {
		start:
			(Number.parseInt(startH, 10) + 12 * +(startAp === "p")) * 60 +
			Number.parseInt(startM ?? "0", 10),
		end:
			(Number.parseInt(endH, 10) + 12 * +(endAp === "p")) * 60 +
			Number.parseInt(endM ?? "0", 10),
	};
};
