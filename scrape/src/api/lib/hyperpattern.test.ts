import * as Vt from "vitest";
import * as P5 from "parse5";
import * as P5Tools from "@parse5/tools";

import * as Hp from "./hyperpattern";

Vt.test("hi", () => {
	const fragment = P5.parseFragment(
		[
			"<table>",
			"<thead><tr><td>foo</td><td>bar</td></tr></thead>",
			"<tbody><tr><td>a</td><td>b</td></tr></tbody>",
			"</table>",
		].join(""),
	).childNodes[0]!;

	const pattern = Hp.element("table")
		.object()
		.childNode(
			"head",
			Hp.element("thead").childNode(
				Hp.element("tr").childNodes(Hp.element("td").textContent()),
			),
		)
		.childNode(
			"body",
			Hp.element("tbody").childNode(
				Hp.element("tr").childNodes(Hp.element("td").textContent()),
			),
		);

	console.log(pattern.parse(fragment));
});
