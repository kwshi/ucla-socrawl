import * as P5 from "parse5";
import * as P5Tools from '@parse5/tools'

export const isTemplateElement = (node: P5Tools.Element): node is P5Tools.Template => node.nodeName === 'template'

export const findElement = (
	node: P5Tools.ParentNode,
  f: (element: P5Tools.Element) => boolean,
): P5Tools.Element | null => {
	if (P5Tools.isElementNode(node) && f(node)) return node;

	for (const child of node.childNodes) {
		if (!P5Tools.isElementNode(child)) continue;
		const result = findElement(child, f);
		if (result) return result;
	}
	return null;
};

export const getElementById = (
	node: P5Tools.ParentNode,
	id: string,
) =>
findElement(node, (elem) => 
  elem.attrs.some(attr => attr.name === 'id' && attr.value === id)
)

export const findElementByTag = (node: P5Tools.ParentNode, tag: string) => findElement(node, elem=> elem.tagName === tag)


export const findElementByTagPath = (node: P5Tools.ParentNode, tagPath: readonly string[]) => {
  let current  = node
  for (const tag of tagPath) {
    const next = findElementByTag(current, tag)
    if (!next) return null
    current = next;
  }
  return current
}

export const getClassNames = (node: P5Tools.Element) => {
	const cs = new Set<string>();
	for (const attr of node.attrs)
		if (attr.name === "class")
			for (const c of attr.value.trim().split(/\s+/)) cs.add(c);
	return cs;
};

export const getText = (node: P5Tools.Node) => {
	const buf: string[] = [];
	const go = (current: typeof node) => {
		if (P5Tools.isTextNode(current)) {
			const text = current.value.trim();
			if (text) buf.push(text);
		}
		if (!P5Tools.isElementNode(current)) return;
		for (const child of current.childNodes) go(child);
	};
	go(node);
	return buf;
};

export const getTextSplitBr = (node: P5Tools.Node) => {
	const lines: string[][] = [];
	let buf: string[] = [];
	const go = (current: typeof node) => {
		if (P5Tools.isTextNode(current)) {
			const text = current.value.trim();
			if (text) buf.push(text);
		}
		if (!P5Tools.isElementNode(current)) return;
		if (current.tagName === "br") {
			lines.push(buf);
			buf = [];
		}
		const classes = getClassNames(current);
		if (classes.has("hide-above-small")) return;
		for (const child of current.childNodes) go(child);
	};
	go(node);
	lines.push(buf);
	return lines;
};

export const isBlankNode = (node: P5Tools.Node) =>
	P5Tools.isTextNode(node) && !node.value.trim();

  export const getChildrenText = (node: P5Tools.ParentNode) => 
node.childNodes.filter(P5Tools.isElementNode).map(getText)

  export const getChildrenTextJoin = (node: P5Tools.ParentNode, sep = '') => 
node.childNodes.filter(P5Tools.isElementNode).map(child => getText(child).join(sep))

export const parseTableRecord = (table: P5Tools.ParentNode) => {
  const children = table.childNodes.filter(P5Tools.isElementNode)
  if (children.length !== 2) throw new Error('table has wrong number of children (expecting 2)')
}
